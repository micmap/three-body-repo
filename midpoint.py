#calculates binary subject to gravity with leapfrog
import numpy as np
import matplotlib.pyplot as plt

plt.rcParams.update({'font.size': 15})

G=0.000118565 #G in units of AU**3/(Mearth * yr**2)
N=3 #number of bodies
dim=3 #number of dimensions




######## ACCELERATION CALCULATION
def accel(x,m):
    a=np.zeros([N,dim],float)
    for i in range(0,N,1):
        for j in range(0,N,1):
            if(i!=j):
                r=np.linalg.norm(x[i,:]-x[j,:])
                a[i,:]=a[i,:]-G*m[j]*(x[i,:]-x[j,:])/r**3
    return a



######### MIDPOINT
def midpoint(x,v,m,h):
    k1x=np.zeros([N,dim],float)
    k1v=np.zeros([N,dim],float)
    k2x=np.zeros([N,dim],float)
    k2v=np.zeros([N,dim],float)
    xmid=np.zeros([N,dim],float)

    a=accel(x,m)
    
    k1x=0.5*h*v
    k1v=0.5*h*a
    xmid=x+k1x

    a=accel(xmid,m)

    k2x=h*(v+k1v)
    k2v=h*a
    
    x+=k2x
    v+=k2v
    return x,v


############
####main####
############

t=0.0 #initial time
tf=5.0 #final time
h=0.01 #timestep


###READ INITIAL CONDITIONS
x=np.zeros([N,dim],float)
v=np.zeros([N,dim],float)
m=np.zeros(N,float)
x[:,0],x[:,1],x[:,2],v[:,0],v[:,1],v[:,2],m=np.genfromtxt("three_bodies.txt",usecols=(0,1,2,3,4,5,6),unpack=True,comments="#")
print(v)



plt.scatter(x[0,0],x[0,1],color="black",s=30)
plt.scatter(x[1,0],x[1,1],color="red",s=10)
plt.scatter(x[2,0],x[2,1],color="blue",s=10)

a=np.zeros([N,dim],float)


###MAIN INTEGRATION
while(t<tf):

    x,v=midpoint(x,v,m,h)

    plt.scatter(x[0,0],x[0,1],color="black",s=1)
    plt.scatter(x[1,0],x[1,1],color="red",s=1)
    plt.scatter(x[2,0],x[2,1],color="blue",s=1)

    t+=h

plt.xlabel("x [au]")
plt.ylabel("y [au]")
plt.tight_layout()
plt.show()

