# Three-body repo

Simple algorithms to integrate the Sun - Mercury - Venus system.


## Description

- three_bodies.txt:

Each column contains the following values

Col. 0, 1, 2: x, y, z components of the initial position in astronomical units (au)

Col. 3, 4, 5: x, y, z components of the initial velocity in au/yr

Col. 6: mass in Earth masses

of the following celestial bodies: 

Line 0: Sun, Line 1: Mercury, Line 2: Venus


- midpoint.py:

Simple python script with midpoint scheme


- midpoint_energy.py:

Simple python script with midpoint scheme. With energy and angular momentum calculation.


- midpoint_energy_adaptive.py:

Simple python script with midpoint scheme. With energy and angular momentum calculation. With simple adaptive time-step.

Have fun.
