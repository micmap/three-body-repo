#calculates binary subject to gravity with leapfrog
import numpy as np
import matplotlib.pyplot as plt
import time

plt.rcParams.update({'font.size': 15})

G=0.000118565 #G in units of AU**3/(Mearth * yr**2)
N=3 #number of bodies
dim=3 #number of dimensions
# choose which integrator you want to use
integrator="midpoint_adaptive"
# possible choices "midpoint", "midpoint_adaptive"


######## ANGMOM CALCULATION
def angmom(x,v,m):
    j=np.zeros([N,dim],float)
    jtot=0.0
    for i in range(N):
        j[i,:]=m[i]*np.cross(v[i,:],x[i,:])
    jtot=np.linalg.norm(sum(j[i,:]))
    return jtot


######## ENERGY CALCULATION
def energy(x,v,m):
    Ek=0.0
    Ep=0.0
    for i in range(N):
        v2=np.linalg.norm(v[i,:])**2
        Ek+=0.5*m[i]*v2
        for j in range(i+1,N):
            r=np.linalg.norm(x[i,:]-x[j,:])
            Ep-=G*m[i]*m[j]/r


    return (Ek+Ep)

######## ACCELERATION CALCULATION
def accel(x,m):
    a=np.zeros([N,dim],float)
    for i in range(0,N,1):
        for j in range(0,N,1):
            if(i!=j):
                r=np.linalg.norm(x[i,:]-x[j,:])
                a[i,:]=a[i,:]-G*m[j]*(x[i,:]-x[j,:])/r**3
    return a




######### MIDPOINT
def midpoint(x,v,m,h):
    k1x=np.zeros([N,dim],float)
    k1v=np.zeros([N,dim],float)
    k2x=np.zeros([N,dim],float)
    k2v=np.zeros([N,dim],float)
    xmid=np.zeros([N,dim],float)

    a=accel(x,m)
    
    k1x=0.5*h*v
    k1v=0.5*h*a
    xmid=x+k1x

    a=accel(xmid,m)

    k2x=h*(v+k1v)
    k2v=h*a
    
    x+=k2x
    v+=k2v
    return x,v,a




############
####main####
############

t=0.0 #initial time
tf=3.0 #final time
h=0.01 #timestep
tol=1.0e-1 #tolerance (for adaptive timestep only)
hmin=1e-7 #minimum allowed h
hmax=0.01 #maximum allowed h

###READ INITIAL CONDITIONS
x=np.zeros([N,dim],float)
v=np.zeros([N,dim],float)
m=np.zeros(N,float)
x[:,0],x[:,1],x[:,2],v[:,0],v[:,1],v[:,2],m=np.genfromtxt("three_bodies.txt",usecols=(0,1,2,3,4,5,6),unpack=True,comments="#")
print(v)



plt.scatter(x[0,0],x[0,1],color="black",s=30)
plt.scatter(x[1,0],x[1,1],color="red",s=10)
plt.scatter(x[2,0],x[2,1],color="blue",s=10)

a=accel(x,m)


E0=energy(x,v,m)
J0=angmom(x,v,m)
temp=[]
de=[]
dj=[]


###MAIN INTEGRATION
start=time.time()

while(t<tf):


    if(integrator=="midpoint"):
        x,v,a=midpoint(x,v,m,h)

        
    elif(integrator=="midpoint_adaptive"):
        xt,vt,at=midpoint(x,v,m,h)
        ht=h
        while((np.linalg.norm(at[1,:]-a[1,:])/np.linalg.norm(at[1,:])>tol) and (ht>hmin)):
            ht=ht*0.1
            xt,vt,at=midpoint(x,v,m,ht)
            print("decrease",ht)
            
        while(np.linalg.norm(at[1,:]-a[1,:])/np.linalg.norm(at[1,:])<(0.1*tol) and (ht<hmax)):
            ht=ht*2.0
            xt,vt,at=midpoint(x,v,m,ht)
            print("increase",ht)
            
        x,v,a,h=np.copy(xt),np.copy(vt),np.copy(at),ht
        #print("stable",t,h)
    else:
        print("The integration scheme you selected is not implemented.")
        print("Implement a new scheme or choose one of the following:")
        print("midpoint, midpoint_adaptive")
        exit()

        
    E=energy(x,v,m)
    de.append((E-E0)/E)
    E0=E
    J=angmom(x,v,m)
    dj.append((J-J0)/J)
    temp.append(t+h)
    J0=J
    t+=h


    plt.scatter(x[0,0],x[0,1],color="black",s=1)
    plt.scatter(x[1,0],x[1,1],color="red",s=1)
    plt.scatter(x[2,0],x[2,1],color="blue",s=1)

end=time.time()
print("Time in main loop",end-start,"s")


plt.xlabel("x [au]")
plt.ylabel("y [au]")
plt.tight_layout()

if(integrator=="midpoint_adaptive"):
    plt.savefig("mid_adapt.png",dpi=300)
elif(integrator=="midpoint"):
    plt.savefig("mid.png",dpi=300)
plt.show()


###plot DE/E
plt.ylabel("$\Delta{}E/E$")
plt.xlabel("Time [yr]")
plt.plot(temp[1:-1],de[1:-1])
plt.tight_layout()

if(integrator=="midpoint_adaptive"):
    plt.savefig("mid_adapt_dE.png",dpi=300)
elif(integrator=="midpoint"):
    plt.savefig("mid_dE.png",dpi=300)

plt.show()


##plot DJ/J
plt.ylabel("$\Delta{}J/J$")
plt.xlabel("Time [yr]")
plt.plot(temp[1:-1],dj[1:-1])
plt.tight_layout()
if(integrator=="midpoint_adaptive"):
    plt.savefig("mid_adapt_dJ.png",dpi=300)
elif(integrator=="midpoint"):
    plt.savefig("mid_dJ.png",dpi=300)


plt.show()


